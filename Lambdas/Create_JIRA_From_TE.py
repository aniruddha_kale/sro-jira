import json
import logging
import boto3
import jiraCoreFunctions as jira

try:
    import requests
except:
    pass

try:
    from botocore.vendored import requests
except:
    pass

paramStoreJIRAToken = "/production/jira-api-token-sro"
jira_user_email_address = "belmont.dashboard_user+sro_jira_automation@roche.com"
project_key = "NAVSROSD"

service_desk_id = "5"
request_type_id = "97"
# 97 - production incidents
# 102 - security incdents on prod - does not work
# 195 - security incidents non prod - does not work

logger = logging.getLogger()
logger.setLevel(logging.INFO)

# Initialize boto3 client at global scope for connection reuse
client = boto3.client('ssm')

def lambda_handler(event, context):
    logger.info('## EVENT')
    logger.info(event)

    # This allows 2 apigateways HTTP Post and REST APi
    try:
        new_event = json.loads(event["body"])
        logger.info(new_event)
    except:
        new_event = event

    if len(new_event) == 0:
        logger.info("Empty Payload. JSON content expected")
        response = {
            "statusCode": 200,
            "body": "Empty Payload. JSON content expected"
        }
        return response

    # Checking if this is a alert clearing call or webhook test call

    if (str(new_event["eventType"]) == "WEBHOOK_TEST"):
        logger.info("TE Webhook test suceed.")
        return 200
    elif (str(new_event["eventType"]) == "ALERT_NOTIFICATION_CLEAR"):
        logger.info("Alert has been cleared - no action performed")
        return 200
    elif (str(new_event["eventType"]) == "ALERT_NOTIFICATION_TRIGGER"):
        logger.info("NEW ThousandEyes Alert has been has been triggered")

    param_details = client.get_parameter(Name=paramStoreJIRAToken, WithDecryption=True)
    jira_token = param_details.get('Parameter')['Value']
    jira_logon = (jira_user_email_address, jira_token)

    testName = new_event["alert"]["testName"]
    ruleName = new_event["alert"]["ruleName"]
    ruleExpression = new_event["alert"]["ruleExpression"]
    dateStart = new_event["alert"]["dateStart"]
    permalink = new_event["alert"]["permalink"]
    product = new_event["alert"]["product"]
    te_alert_id = str(new_event["alert"]["alertId"])

    logger.info("Test Name: " + testName)
    logger.info("Alert Rule Name: " + ruleName)
    logger.info("Date Start: " + dateStart)
    logger.info("Alert URL: " + permalink)
    logger.info("Component: " + product)
    logger.info("AlertID: " + te_alert_id)

    summary_string = "ThousandEyes %s FAILED" % (testName)
    description_string = "ThousandEyes rule %s was triggered due to test \"%s\" that FAILED on %s. \nPlease check alerts data at %s ." % (
    ruleName, testName, dateStart, permalink)
    priority_string = "High"
    label_strings = ""
    finding_type_string = "Internal Finding"
    current_behavior_string = ruleExpression
    expected_behavior_string = "Test to be passing on all agents."
    steps_string = "Login to ThousandEyes and rerun the test please."
    environment_strings = "Production"
    cir_id_string = ""
    src_of_findings_string = "ThousandEyes"
    component_strings = product  #TODO - Components could be based on account name if TE sents such info in alert
    os_type_string = ""
    os_version_string = ""
    browser_type_string = ""
    browser_version_string = ""
    prod_region_string = ""

    request_key = jira.create_jira_sd_ticket(jira_logon, service_desk_id, request_type_id, summary_string,
                                                          description_string,
                                                          priority_string, te_alert_id, component_strings,
                                                          label_strings, finding_type_string,
                                                          current_behavior_string, expected_behavior_string,
                                                          steps_string, environment_strings,
                                                          cir_id_string, src_of_findings_string, os_type_string,
                                                          os_version_string, browser_type_string,
                                                          browser_version_string, prod_region_string)

    if request_key is not None:
        logger.info("Ticket created! %s" % (request_key))
    else:
        logger.info("DEBUG: Ticket create failure!")

    return {
        "statusCode": 200,
        "headers": {
            "Content-Type": "application/json"
        },
        "body": json.dumps({
            "JIRA Ticket": request_key
        })
    }
