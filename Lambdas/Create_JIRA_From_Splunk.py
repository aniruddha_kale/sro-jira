import json
import logging
import boto3
import hashlib
import jiraCoreFunctions

try:
    import requests
except:
    pass

try:
    from botocore.vendored import requests
except:
    pass

paramStoreJIRAToken = "/production/jira-api-token-sro"
jira_user_email_address = "belmont.dashboard_user+sro_jira_automation@roche.com"
project_key = "NAVSROSD"

service_desk_id = "5"
request_type_id = ""
# 97 - production incidents
# 102 - security incdents on prod - does not work
# 195 - security incidents non prod - does not work

logger = logging.getLogger()
logger.setLevel(logging.INFO)

# Initialize boto3 client at global scope for connection reuse
client = boto3.client('ssm')

def lambda_handler(event, context):
    logger.info('## EVENT')
    logger.info(event)

    # This allows 2 apigateways HTTP Post and REST APi
    try:
        new_event = json.loads(event["body"])
        logger.info(new_event)
    except:
        new_event = event


    if len(new_event) == 0:
        logger.info("Empty Payload. JSON content expected")
        response = {
            "statusCode": 200,
            "body": "Empty Payload. JSON content expected"
        }

        return response

    param_details = client.get_parameter(Name=paramStoreJIRAToken, WithDecryption=True)
    jira_token = param_details.get('Parameter')['Value']
    jira_logon = (jira_user_email_address, jira_token)

    summary_string = new_event["summary"]
    description_string = new_event["description"]
    idf_string = new_event["identifier"]
    environment_strings = new_event["environment"]
    priority_string = new_event["priority"]
    component_strings = new_event["component"]
    finding_type_string = new_event["findingType"]
    src_of_findings_string = new_event["sourceOfFinding"]

    if new_event.get('requestTypeID') is None:
        response = {
            "statusCode": 500,
            "body": "Missing parameter in payload: requestTypeID can not be empty"
        }
        return response

    request_type_id = new_event["requestTypeID"]

    if request_type_id == "97":
        label_strings = new_event["labels"]
    else:
        label_strings = ""

    current_behavior_string = ""
    expected_behavior_string = ""
    steps_string = ""
    cir_id_string = ""
    os_type_string = ""
    os_version_string = ""
    browser_type_string = ""
    browser_version_string = ""
    prod_region_string = ""
    te_alert_id = ""

    idf_string_encoded = "ISH_%s" % (hashlib.sha224(idf_string.encode('utf-8')).hexdigest())

    description_string = "%s\n\n[Ticket Tracker: DO NOT EDIT: %s]" % (description_string, idf_string_encoded)

    #comment_string = "Issue still exist - adding a comment instead of creating new ticket."

    request_key = jiraCoreFunctions.search_jira_ticket(jira_logon, project_key, idf_string)

    if request_key is not None:
        logger.info("DEBUG: Found ticket request_key=%s" % (request_key))
        comment_url = jiraCoreFunctions.comment_jira_sd_ticket(jira_logon, request_key, description_string, False)
        logger.info("DEBUG: comment_url=%s" % (comment_url))
    else:
        logger.info("DEBUG: Ticket not found, Create Ticket")
        request_key = jiraCoreFunctions.create_jira_sd_ticket(jira_logon, service_desk_id, request_type_id, summary_string, description_string,
                                                              priority_string, te_alert_id, component_strings, label_strings, finding_type_string,
                                                              current_behavior_string, expected_behavior_string, steps_string, environment_strings,
                                                              cir_id_string, src_of_findings_string, os_type_string, os_version_string, browser_type_string,
                                                              browser_version_string, prod_region_string)
        if request_key is not None:
            logger.info("Ticket created! %s" % (request_key))
        else:
            logger.info("DEBUG: Ticket create failure!")

    # TODO: Implement your alert action logic here
    return {
        "statusCode": 200,
        "headers": {
            "Content-Type": "application/json"
        },
        "body": json.dumps({
            "JIRA Ticket": request_key
        })
    }
