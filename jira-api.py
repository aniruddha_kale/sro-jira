import json
import requests
import re

try:
    from excludes.headers import headers
except:
    headers = {
        "Accept": "application/json",
        "Content-Type": "application/json",
        "Authorization": "Basic 1Password"
    }

base_url = "https://navifypoc.atlassian.net"
api_url = "/rest/api/3/"
component_url = "component"
groups_url = "group"
groups_user_url = groups_url + "/user"
search_user_url = "user/search"


class MyDictionary(dict):

    # __init__ function
    def __init__(self):
        self = dict()

    # Function to add key:value
    def add(self, key, value):
        self[key] = value

    # Function to add multiple values to a key
    def add_more(self, key, value):
        self[key].append(value)


def run_query_post(url, data, dry_run=False):
    # type: (string, json, bool) -> string

    response = ""

    if not dry_run:

        try:
            # response = requests.post(url, auth=(username, password))
            response = requests.post(url, headers=headers, json=data)

            if bool(re.match(re.compile("20."), str(response.status_code))):
                # if dry_run:
                print("Response Code:", response)
                # return account_group
            else:
                print("FAILED:", response, response.content)
                # exit(1)

            # parsed_json = json.loads(response.content)
        except Exception as e:
            print("FAILED:", e)
            exit(1)
    else:
        response = url + "\n" + str(test_config) + "\n"
        print("Dry run mode, printing query payload:", response)

    return response


def run_query_get(url, dry_run=False):
    # this function is responsible for sending queries to Thousand API

    if not dry_run:
        try:
            response = requests.get(url, headers=headers)

            if bool(re.match(re.compile("20."), str(response.status_code))):
                if dry_run:
                    print("Response Code:", response)
                # return account_group
            else:
                print("FAILED:", response)

            parsed_json = json.loads(response.content)

        except Exception as e:
            print("FAILED:", e)
            exit(1)
        else:
            return parsed_json
    else:
        response = url
        print("Dry run mode, printing query payload:", response)
        return response


def create_component(dry_run=False, **kwargs):

    try:
        component_name = kwargs['component_name']
    except:
        print("Error - missing component name.")
        exit(1)

    try:
        component_description = kwargs['component_description']
    except:
        print("Error - missing component description.")
        exit(1)

    try:
        jira_project = kwargs['jira_project']
    except:
        jira_project = "NAVSROSD"
        print("INFO - missing jira project name - going with default: %s" % jira_project)

    payload = {
      "isAssigneeTypeValid": False,
      "name": component_name,
      "description": component_description,
      "project": jira_project,
      "assigneeType": "PROJECT_DEFAULT",
      "leadAccountId": ""
    }

    url = base_url + api_url + component_url

    response = run_query_post(url, payload)

    parsed_json = json.loads(response.content)

    if dry_run:
        json_formatted_str = json.dumps(parsed_json, indent=2)

        print(json_formatted_str)

    print("Component ID: %s" % (parsed_json['id']))

    # returns component ID
    return parsed_json['id']


def create_group(group_name, dry_run=False):

    payload = {
      "name": group_name
    }

    url = base_url + api_url + groups_url

    response = run_query_post(url, payload)

    parsed_json = json.loads(response.content)

    if dry_run:
        json_formatted_str = json.dumps(parsed_json, indent=2)

        print(json_formatted_str)

    return response.content


def add_access_group_to_project(dry_run=False, **kwargs):
    # once group is created we need to add it to NAVSROSD project so the people in the group would have access to it

    try:
        jira_project = kwargs['jira_project']
    except:
        jira_project = "NAVSROSD"
        print("WARN - missing jira project name - going with default: %s" % jira_project)

    try:
        group_name = kwargs['group_name']
    except:
        print("Error - missing group name.")
        exit(1)

    payload = {
       "group": [
          group_name
       ]
    }

    url = base_url + api_url + "project/" + jira_project + "/role/10133"

    response = run_query_post(url, payload)

    parsed_json = json.loads(response.content)

    if dry_run:
        json_formatted_str = json.dumps(parsed_json, indent=2)

        print(json_formatted_str)

    return response.content


def add_user_to_group(dry_run=False, **kwargs):
    # adding users to a group, users needs to be provided as a list of emails

    try:
        group_name = kwargs['group_name']
    except:
        print("Error - missing group name.")
        exit(1)

    try:
        users_email_list = kwargs['users_list']
    except:
        print("Error - missing user list.")
        exit(1)

    print("Adding users to a JIRA group: %s" % group_name)

    # this is a remedy if there is just one user in the tupple
    if type(users_email_list) is not tuple:
        users_email_list = (users_email_list,)

    for user_email in users_email_list:

        user_id = get_user_id(user_email, dry_run)

        payload = {
            "accountId": user_id[0]
        }

        url = base_url + api_url + groups_user_url + "?groupname=" + group_name

        if dry_run:
            print("Payload: %s" % payload)
            print("URL: %s" % url)

        response = run_query_post(url, payload)

        parsed_json = json.loads(response.content)

        if dry_run:
            json_formatted_str = json.dumps(parsed_json, indent=2)

            print(json_formatted_str)

    return True

def get_user_id(users_email_list, dry_run=False):
    # getting users id based on their emails

    users_id_list = []

    if not users_email_list:
        print("Error - missing user list.")
        exit(1)

    # this is a remedy if there is just one user in the tuple
    if type(users_email_list) is not tuple:
        users_email_list = (users_email_list,)

    for user_email in users_email_list:

        if len(user_email) > 1:

            print("\nGetting ID of a user: %s" % user_email)

            url = base_url + api_url + search_user_url + "?query=" + user_email

            parsed_json = run_query_get(url)

            if not parsed_json:
                print("ERROR - user not found.")
                continue

            for json_record in parsed_json:

                if json_record['accountType'] == "atlassian":
                    users_id_list.append(json_record['accountId'])

                    print("User ID: %s" % json_record['accountId'])

            if dry_run:
                json_formatted_str = json.dumps(parsed_json, indent=2)

                print(json_formatted_str)

    if dry_run:
        print("List of user ID's: %s" % users_id_list)

    return users_id_list

def main():
    product_name = "NOH"

    # group for access management for NAVSROSD project
    access_group_name = "NAVSROSD-AccessRole-" + product_name

    # Creating component
    component_name = "Navify Oncology Hub"
    create_component(component_name=component_name, component_description="Component for a project " + component_name)

    # Crating JIRA group
    create_group(access_group_name)

    # Adding access_group_name group to NAVSROSD in order to provide access
    add_access_group_to_project(group_name=access_group_name)

    # adding users to a group based on email list
    email_list_access_group = ("magdalena.alomar_adrover@roche.com")

    # Getting user IDs based on email address - if you need JIRA user ID for some reason
    email_list = email_list_access_group
    # get_user_id(email_list, dry_run=True)

    # Adding users to NAVSROSD Access Group
    add_user_to_group(dry_run=False, group_name=access_group_name, users_list=email_list_access_group)


if __name__ == '__main__':
    main()